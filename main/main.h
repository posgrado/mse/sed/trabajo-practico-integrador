/** \file	main.h
 *  Mar 2022
 *  Maestría en SIstemas Embebidos / Sistemas embebidos distribuidos
 *  \brief	Contiene las bibliotecas y definiciones generales de configuración
 */

#ifndef MAIN_H_
#define MAIN_H_


/* Configuración general  */
#include "config.h"

/* Nivel de abstracción APID */
#include "mqtt.h"
#include "wifi.h"
#include "io.h"
#include "crono.h"
#include "sd.h"

#endif /* MAIN_H_ */
