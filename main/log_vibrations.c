/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "main.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include <driver/i2c.h>
#include "esp_log.h"
#include "led_strip.h"
#include "sdkconfig.h"

/* Use project configuration menu (idf.py menuconfig) to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO CONFIG_BLINK_GPIO

#define TASK_ADQ_STACK_SIZE 4096
#define PIN_SDA 25
#define PIN_CLK 26
#define I2C_ADDRESS 0x68 // I2C address of MPU6050
#define MPU6050_ACCEL_XOUT_H 0x3B
#define MPU6050_PWR_MGMT_1 0x6B

static uint8_t s_led_state = 0;

#ifdef CONFIG_BLINK_LED_RMT
static led_strip_t *pStrip_a;

static void blink_led(void)
{
    /* If the addressable LED is enabled */
    if (s_led_state)
    {
        /* Set the LED pixel using RGB from 0 (0%) to 255 (100%) for each color */
        pStrip_a->set_pixel(pStrip_a, 0, 16, 16, 16);
        /* Refresh the strip to send data */
        pStrip_a->refresh(pStrip_a, 100);
    }
    else
    {
        /* Set all LED off to clear all pixels */
        pStrip_a->clear(pStrip_a, 50);
    }
}

static void configure_led(void)
{
    static const char *TAG = __FUNCTION__;
    ESP_LOGI(TAG, "Example configured to blink addressable LED!");
    /* LED strip initialization with the GPIO and pixels number*/
    pStrip_a = led_strip_init(CONFIG_BLINK_LED_RMT_CHANNEL, BLINK_GPIO, 1);
    /* Set all LED off to clear all pixels */
    pStrip_a->clear(pStrip_a, 50);
}

#elif CONFIG_BLINK_LED_GPIO

static void blink_led(void)
{
    /* Set the GPIO level according to the state (LOW or HIGH)*/
    gpio_set_level(BLINK_GPIO, s_led_state);
}

static void configure_led(void)
{
    static const char *TAG = __FUNCTION__;
    ESP_LOGI(TAG, "Example configured to blink GPIO LED!");
    gpio_reset_pin(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
}

#endif

xSemaphoreHandle xSemaphoreStop = NULL;
xQueueHandle xQueueStart = NULL;

void vTaskDataAdq(void *pvParameters)
{
    static const char *TAG = __FUNCTION__;

    TickType_t xLastWakeTime;

    /* Inicializaciones */
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = PIN_SDA;
    conf.scl_io_num = PIN_CLK;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = 100000;
    ESP_ERROR_CHECK(i2c_param_config(I2C_NUM_0, &conf));
    ESP_ERROR_CHECK(i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0));

    i2c_cmd_handle_t cmd;
    vTaskDelay(200 / portTICK_PERIOD_MS);

    cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
    i2c_master_write_byte(cmd, MPU6050_ACCEL_XOUT_H, 1);
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);

    cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
    i2c_master_write_byte(cmd, MPU6050_PWR_MGMT_1, 1);
    i2c_master_write_byte(cmd, 0, 1);
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);

    uint8_t data[14];

    short accel_x;
    short accel_y;
    short accel_z;

    for (;;)
    {
        uint8_t seconds_to_adq;
        if (xQueueReceive(xQueueStart, &seconds_to_adq, portMAX_DELAY) == pdPASS)
        {
            // Initialise the xLastWakeTime variable with the current time.
            xLastWakeTime = xTaskGetTickCount();
            ESP_LOGI(TAG, "Data acquisition for %d seconds", seconds_to_adq);
            ESP_LOGI(TAG, "Data acquisition started at: %d", xTaskGetTickCount() / portTICK_PERIOD_MS);
        }
        char filename[13] = "";
        sprintf(filename, "A%07d.csv", xTaskGetTickCount() / portTICK_PERIOD_MS);
        ESP_LOGI(TAG, "Filename: %s", filename);
        FILE *file = SD_open(filename, "w");
        SD_puts(file, "timestamp_ms,accel_x,accel_y,accel_z\n");
        char line[] = "0000000,000000,000000,000000\n";
        for (size_t i = 0; i < seconds_to_adq * 100; i++)
        {
            if(xSemaphoreTake(xSemaphoreStop, 0) == pdTRUE){
                break;
            }
            // Tell the MPU6050 to position the internal register pointer to register
            // MPU6050_ACCEL_XOUT_H.
            cmd = i2c_cmd_link_create();
            ESP_ERROR_CHECK(i2c_master_start(cmd));
            ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_WRITE, 1));
            ESP_ERROR_CHECK(i2c_master_write_byte(cmd, MPU6050_ACCEL_XOUT_H, 1));
            ESP_ERROR_CHECK(i2c_master_stop(cmd));
            ESP_ERROR_CHECK(i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_PERIOD_MS));
            i2c_cmd_link_delete(cmd);

            cmd = i2c_cmd_link_create();
            ESP_ERROR_CHECK(i2c_master_start(cmd));
            ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (I2C_ADDRESS << 1) | I2C_MASTER_READ, 1));

            // Wait for the next cycle.
            vTaskDelayUntil(&xLastWakeTime, 10 / portTICK_RATE_MS);

            ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data, 0));
            ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data + 1, 0));
            ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data + 2, 0));
            ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data + 3, 0));
            ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data + 4, 0));
            ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data + 5, 1));
            accel_x = (data[0] << 8) | data[1];
            accel_y = (data[2] << 8) | data[3];
            accel_z = (data[4] << 8) | data[5];
            ESP_LOGI(TAG, "[ %d ] accel_x: %d, accel_y: %d, accel_z: %d", xTaskGetTickCount() / portTICK_PERIOD_MS, accel_x, accel_y, accel_z);
            sprintf(line, "%07d,%06d,%06d,%06d\n", xTaskGetTickCount() / portTICK_PERIOD_MS, accel_x, accel_y, accel_z);
            SD_puts(file, line);
            // i2c_master_read(cmd, data, sizeof(data), 1);
            ESP_ERROR_CHECK(i2c_master_stop(cmd));
            ESP_ERROR_CHECK(i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_PERIOD_MS));
            i2c_cmd_link_delete(cmd);
        }
        SD_close(file);
        SD_unmount();
    }
}

void app_main(void)
{
    static const char *TAG = __FUNCTION__;
    /* Configure the peripheral according to the LED type */
    configure_led();
    xSemaphoreStop = xSemaphoreCreateBinary();
    xQueueStart = xQueueCreate(2, sizeof(uint8_t));

    WIFI_init();
    MQTT_init();
    MQTT_subscribe("control/start");
    MQTT_subscribe("control/stop");
    MQTT_subscribe("alarm/clear_all");

    BaseType_t xReturned;
    TaskHandle_t xHandle = NULL;
    /* Create the task, storing the handle. */
    xReturned = xTaskCreatePinnedToCore(
        vTaskDataAdq,               /* Function that implements the task. */
        "Data Adquisition task",    /* Text name for the task. */
        TASK_ADQ_STACK_SIZE,        /* Stack size in words, not bytes. */
        NULL,                       /* Parameter passed into the task. */
        tskIDLE_PRIORITY + 2,       /* Priority at which the task is created. */
        &xHandle,                   /* Used to pass out the created task's handle. */
        1);                         /* Task affinity. 0 = CPU0, 1 = CPU1 */
    configASSERT(xReturned == pdPASS);

    while (1)
    {
        ESP_LOGI(TAG, "Turning the LED %s!", s_led_state == true ? "ON" : "OFF");
        blink_led();
        /* Toggle the LED state */
        s_led_state = !s_led_state;
        vTaskDelay(CONFIG_BLINK_PERIOD / portTICK_PERIOD_MS);
    }
}
